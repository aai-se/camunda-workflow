package aaicr.utils;

import java.net.URL;

import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

import aaicr.utils.HTTPOperations;



public class CROperations {
	
	

	
	private String botId ;
	private String botName  ;
	private String poolId;
	private String poolName;
	private String deploymentId;
	private Map<String,Object> inputVariables;

    private String crurl ;
    private String callbackurl ;
	private String crapikey;
	private String cruser ;
	private String token ;
	private Integer userID;
	
	public CROperations() {
	
	}
	

	public CROperations setCrurl(String  url) {
		this.crurl = url;
		return this;
		
	}
	
	public CROperations setCallbackurl(String  url) {
		this.callbackurl = url;
		return this;
		
	}
	
	
	public CROperations setCruser(String user) {
		this.cruser = user;
		return this;
	}
	
	
	
	public CROperations setCrapikey(String apikey) {
		this.crapikey = apikey;
		return this;
	}
	
	public CROperations setPoolname(String poolname) {
		this.poolName = poolname;
		return this;
	}
	
	
	public CROperations setBotname(String botname) {
		this.botName = botname;
		return this;
	}
	
	public CROperations setInputVariables(Map<String,Object> variables) {
		this.inputVariables = variables;
		return this;
	}
	
	public String getBotId() {
		return this.botId;
	}
	
	
	public Integer getUserId() {
		return this.userID;
	}
	
	public String getDeploymentId() {
		return this.deploymentId;
	}
	
	public String getPoolId() {
		return this.poolId;
	}
	
	public String getCallbackURL() {
		return this.callbackurl;
	}
	
	

	
	
	
	public String getAutomationStatus() throws Exception {
  
		String automationStatus = "UNKNOWN";
		URL url = new URL(this.crurl+"/v2/activity/list");
		JSONObject body = new JSONObject();
		JSONObject filter = new JSONObject();
		filter.put("operator", "eq");
		filter.put("field","deploymentId");
		filter.put("value", this.deploymentId);
		body.put("filter",filter);
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			 JSONArray statusJson = (JSONArray)json.get("list");
			if (!statusJson.isEmpty()) {
				 JSONObject automation = (JSONObject)statusJson.get(0);
				 automationStatus = automation.getString("status");
			}
				
		}
		
		return automationStatus;
	}

	
	

	
	
	
	public void getBotbyName() throws Exception {

		JSONArray botsJson = null;
		URL url = new URL(this.crurl+"/v2/repository/file/list");
		JSONObject body = new JSONObject(); 
		JSONObject filter = new JSONObject();
		filter.put("operator", "eq");
		filter.put("value", this.botName);
		filter.put("field", "name");
		body.put("filter",filter);
		JSONObject page = new JSONObject();
		page.put("offset", "0");
		page.put("length", "10");
		body.put("page",page);
		String result = HTTPOperations.POSTRequest(url,this.token,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject json = new JSONObject(result);
			botsJson = (JSONArray)json.get("list");
		}

    	if (botsJson != null) {
    		botsJson.forEach(item -> {
    			JSONObject jsonobj = (JSONObject) item;
    			String botname = jsonobj.getString("name");
    			String id = jsonobj.getString("id");
    			
    			if (!botname.contains(".png")) {
    				this.botId = id;
    				this.botName = botname;
    			}
    	  });
    	}
	}

	

	public void authenticate() throws Exception {
		URL url = new URL(this.crurl+"/v1/authentication");
		JSONObject body = new JSONObject();
		body.put("username", this.cruser);
		body.put("apikey", this.crapikey);
		String result = HTTPOperations.POSTRequest(url,null,body);
		if (!result.contains("POST NOT WORKED")) {
			JSONObject Json = new JSONObject(result);
			this.token = Json.get("token").toString();
			this.userID = ((JSONObject)Json.get("user")).getInt("id");
		
		}
	}	 
	
	
	
	public String deploybyName() throws Exception {
		JSONObject automation = null;
		if ((this.botName != null) && (this.cruser != null) && (this.poolId != null)) {
			URL url = new URL(this.crurl+"/v3/automations/deploy");
			JSONObject body = new JSONObject();
			body.put("fileId",this.botId);
			JSONArray userIds = new JSONArray();
			userIds.put(this.userID);
			body.put("runAsUserIds", userIds);	
			JSONArray poolIds = new JSONArray();
			poolIds.put(this.poolId);
			body.put("poolIds", poolIds);
			
			
			if (this.callbackurl != null) {
				JSONObject callback = new JSONObject();
				callback.put("url", this.callbackurl);
				body.put("callbackInfo", callback);
			}
			
			
			
			if (this.inputVariables.size() > 0)
			{
				JSONObject botInput = new JSONObject();
				for (Entry<String,Object> inputvariable : this.inputVariables.entrySet()) {
					JSONObject variable = new JSONObject();
					String classname = inputvariable.getValue().getClass().getSimpleName();
					switch (classname) {
						case "String":
							variable.put("type", "STRING");
							variable.put("string",inputvariable.getValue());
							break;
						case "Boolean":
							variable.put("type", "BOOLEAN");
							variable.put("boolean",inputvariable.getValue());
							break;
						case "Double":
							variable.put("type", "NUMBER");
							variable.put("number",inputvariable.getValue());
							break;
						case "Integer":
							variable.put("type", "NUMBER");
							variable.put("number",inputvariable.getValue());
							break;

						default:
							break;
					}
					
					botInput.put(inputvariable.getKey(), variable);
				}
				
				body.put("botInput", botInput);
				
			}
			
						
			
			String result = HTTPOperations.POSTRequest(url,this.token,body);
			if (!result.contains("POST NOT WORKED")) {
				automation = new JSONObject(result);
				this.deploymentId = automation.getString("deploymentId");
			}
			
		} 
		
		return this.deploymentId;
	}
	
	
	public void getPoolbyName() throws Exception {
		JSONArray pools = null;
		if ((this.poolName != null)) {
			URL url = new URL(this.crurl+"/v2/devices/pools/list");
			JSONObject body = new JSONObject(); 
			JSONObject filter = new JSONObject();
			filter.put("operator", "eq");
			filter.put("value", this.poolName);
			filter.put("field", "name");
			body.put("filter",filter);
			JSONObject page = new JSONObject();
			page.put("offset", "0");
			page.put("length", "100");
			body.put("page",page);
			String result = HTTPOperations.POSTRequest(url,this.token,body);
			if (!result.contains("POST NOT WORKED")) {
				JSONObject json = new JSONObject(result);
				pools = (JSONArray)json.get("list");
			}

	    	if (pools != null) {
	    		pools.forEach(item -> {
	    			JSONObject jsonobj = (JSONObject) item;
	    			String botname = jsonobj.getString("name");
	    			String id = jsonobj.getString("id");
	    			
	    			if (!botname.contains(".png")) {
	    				this.poolId = id;
	    				this.poolName = botname;
	    			}
	    	  });
	    	}
			
		} 
	}

}
