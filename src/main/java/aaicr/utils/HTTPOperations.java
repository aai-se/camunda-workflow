package aaicr.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

public class  HTTPOperations {

	 public static String  POSTRequest(URL posturl, String token, JSONObject body) throws UnsupportedEncodingException, IOException  {

		int responseCode = 0;
	    HttpURLConnection postConnection = null;
		try {
			postConnection = (HttpURLConnection) posturl.openConnection();
		    postConnection.setRequestMethod("POST");
	        if (token != null)
	        {
	    	    postConnection.setRequestProperty("x-authorization", token);
	        }


		    postConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
		    postConnection.setDoOutput(true);
		    OutputStream os = postConnection.getOutputStream();

		    os.write(body.toString().getBytes());
		    
		    os.flush();

		    os.close();

		    responseCode = postConnection.getResponseCode();
		    
		} catch (IOException e) {


		}

	    if (responseCode == HttpURLConnection.HTTP_OK) { //success

	        BufferedReader in = new BufferedReader(new InputStreamReader(

	            postConnection.getInputStream(),StandardCharsets.UTF_8.name()));

	        String inputLine;

	        StringBuffer response = new StringBuffer();

	        while ((inputLine = in .readLine()) != null) {

	            response.append(inputLine);

	        } in .close();

	        
	        return  response.toString() ;

	    } else {
 
			if (responseCode > 0)
			{	
			}
	
	        return "POST NOT WORKED "+ Integer.valueOf(responseCode).toString();

	    }

	}

}
