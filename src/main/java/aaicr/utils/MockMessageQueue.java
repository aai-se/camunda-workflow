package aaicr.utils;


import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * <p>This class is supposed to mock the queuing infrastructure between the process engine 
 * and the service implementation. In a real life scenario, we would use reliable queuing 
 * middleware such as a transactional messaging system (typically JMS).</p>
 *
 */
public class MockMessageQueue {
  
	  private final static Logger LOGGER = Logger.getLogger("CAMUNDA Requests");
	
  /** a Message with payload */
  public static class Message {
    
    protected Map<String, Object> payload = new HashMap<String, Object>();

    public Message(Map<String, Object> payload) {
      this.payload = payload;
    }
    
    public Map<String, Object> getPayload() {
      return payload;
    }
     
  }
  
  protected List<Message> queue = new LinkedList<MockMessageQueue.Message>();
  
  public final static MockMessageQueue INSTANCE = new MockMessageQueue();
  
  public void send(Message m) {
    queue.add(m);
  }
  
  public Message findMessage(String deploymentId, boolean remove) {
	   Message message = null;
	    for (Iterator iterator = queue.iterator(); iterator.hasNext();) {
			message = (Message) iterator.next();
			if (message.getPayload().get("deplyomentId").toString().equals(deploymentId)) {
				break;
			}
	    }
			
		if (message!= null && remove)
		{
			   LOGGER.info("Queue Lengh before remove'" + queue.size() + "'...");
			queue.remove(message);
			   LOGGER.info("Queue Lengh after remove'" + queue.size() + "'...");
			
		}
	    return message ;
  }
  
  

  
  
  public Message getNextMessage() {
    return queue.remove(0);
  }

}


