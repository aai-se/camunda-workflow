package aaicr;

import java.util.Collections;
import java.util.Map;

import org.camunda.bpm.engine.ProcessEngine;

import aaicr.utils.MockMessageQueue.Message;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;

@ApplicationPath("/rpacallback")
public class BotListener extends Application  {

	
	// The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media
    // type "text/plain"
    @Produces("text/plain")
    public String getClichedMessage() {
        // Return some cliched textual content
        return "Hello World";
    }

	public void invoke(Message message, ProcessEngine processEngine) {

		 Map<String, Object> requestPayload = message.getPayload();
		    // the execution id is used as correlation identifier
		String executionId = (String) requestPayload.get("executionId");

		    // Send the callback to the process engine. In this example we send
		    // a synchronous callback. We could also send an asynchronous callback
		    // using a message queue.
		Map<String, Object> callbackPayload = Collections.<String,Object>singletonMap("botStatus","received");
		processEngine.getRuntimeService().signal(executionId, callbackPayload);

		 }
		}